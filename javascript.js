"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */
const gBASE_URL = "https://pucci-mart.onrender.com/api";
const gTotalCard = 24;
let gPerpage = 8;
let gTotalPages = Math.ceil(gTotalCard / gPerpage);
let gPagenum = 1;
// callAPI(1, 8)
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
$(document).ready(function () {
    //giao diện 8 bản ghi đầu tiên của trang 1
    makeCall(1);
})

// Hàm action cho mỗi nút phân trang
function makeCall(gPagenum) {
    createpagination(gPagenum);
    callAPI(gPagenum, gPerpage)
}
/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Gọi API load ra giao diện 8 bản ghi đầu tiên của trang 1
function callAPI(gPagenum, gPerpage) {
    const vQueryParams = new URLSearchParams({
        "_limit": gPerpage,
        "_page": gPagenum
    });

    $.ajax({
        type: 'get',
        url: gBASE_URL + '/pets?' + vQueryParams.toString(),
        dataType: 'json',
        success: function (paramData) {
            $('.row-card-pet-container').html('');
            for (var index = 0; index < paramData.rows.length; index++) {
                if (Number(paramData.rows[index].discount) > 0) {
                    $('.row-card-pet-container').append(
                        `
                     <div class="text-center card-item-wrap col-md-3 mb-5">
                     <div class="card-img">
                         <img src=${paramData.rows[index].imageUrl} alt="">
                         <div class="tag-discount text-center">
                             <p>-${paramData.rows[index].discount}%</p>
                         </div>
                     </div>
                     <div class="card-body text-center d-flex flex-column">
                         <h5>${paramData.rows[index].name}</h5>
                         <p>${paramData.rows[index].description}</p>
                         <div class="detail-price">
                             <span class="price">$${paramData.rows[index].promotionPrice}</span><span class="cross-price">$${paramData.rows[index].price}</span>
                         </div>
                     </div>
                 </div>
                     `
                    )
                } else {
                    $('.row-card-pet-container').append(`
                     <div class=" text-center card-item-wrap col-md-3 mb-5">
                        <div class="card-img">
                           <img src=${paramData.rows[index].imageUrl} alt="">
                        </div>
                        <div class="card-body text-center d-flex flex-column gap-2">
                           <h5>${paramData.rows[index].name}</h5>
                           <p>${paramData.rows[index].description}</p>
                            <div class="detail-price">
                               <span class="price">$${paramData.rows[index].promotionPrice}</span><span class="cross-price">$${paramData.rows[index].price}</span>
                            </div>
                        </div>
                     </div>
                   `)
                }
            }

        },
        error: function () {
            $('.row-card-pet-container').html('error')
        }
    });

}

// Hàm tạo thanh phân trang
function createpagination(gPagenum) {
    $('#page-container').html('');
    // Nếu tran hiện tại là trang 1 thì nút Prev sẽ bị disable
    if (gPagenum == 1) {
        $('#page-container').append(`
                <li class="page-item disabled li-previous">
                    <a class="page-link a-previous " href="javascript:void(0)" aria-label="Previous">
                        <span aria-hidden="true"><i class="fa-solid fa-circle-chevron-left"></i></span>
                    </a>
                </li>
`
        )
    } else {
        $('#page-container').append(`
                <li class="page-item " onclick="makeCall(${gPagenum - 1})">
                    <a class="page-link a-previous" href="javascript:void(0)" aria-label="Previous">
                         <span aria-hidden="true"><i class="fa-solid fa-circle-chevron-left"></i></span>
                    </a>
                </li>
            `)
    }

    // Nếu trang hiện tại là trang cuối cùng thì nút Next sẽ bị disable
    if (gPagenum == gTotalPages) {
        $('#page-container').append(`
                <li class="page-item disabled li-next">
                    <a class="page-link a-next" href="javascript:void(0)" aria-label="Next">
                        <span aria-hidden="true"><i class="fa-solid fa-circle-chevron-right"></i></span>
                    </a>
                </li>
                `
        )
    }
    else {
        $('#page-container').append(`
                <li class="page-item li-next" onclick = 'makeCall(${gPagenum + 1})'>
                    <a class="page-link a-next" href="javascript:void(0)" aria-label="Next">
                        <span aria-hidden="true"><i class="fa-solid fa-circle-chevron-right"></i></span>
                    </a>
                </li>
                `
        )
    }
}
