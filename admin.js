"use strict";
/*** REGION 1 - Global variables - Vùng khai báo biến, hằng số, tham số TOÀN CỤC */

var gBASE_URL = "https://pucci-mart.onrender.com/api";

let gPET = ['name', 'type', 'description', 'imageUrl', 'price', 'promotionPrice', 'discount', 'createdAt', 'updatedAt', 'id']

const gNAME_COL = 0;
const gTYPE_COL = 1;
const gDESCRIPTION_COL = 2;
const gIMAGE_URL_COL = 3;
const gPRICE_COL = 4;
const gPROMOTION_PRICE_COL = 5;
const gDISCOUNT_COL = 6;
const gCREATE_DATE = 7;
const gUPDATE_DATE = 8;
const gACTION = 9;
let gPET_LIST;
let gPET_ID;
let gPetTable = $("#example1").DataTable({
    // "responsive": true,
    "lengthChange": false, "autoWidth": false,
    "buttons": ["excel", "pdf", "print",],
    data: gPET_LIST,
    order: [[gUPDATE_DATE, 'desc']],
    columns: [
        {
            data: gPET[gNAME_COL],
            title: 'Name',
        },
        {
            data: gPET[gTYPE_COL],
            title: 'Type',
        },
        {
            data: gPET[gDESCRIPTION_COL],
            title: 'Description',
        },
        {
            data: gPET[gIMAGE_URL_COL],
            className: 'text-center',
            title: 'Image',
        },
        {
            data: gPET[gPRICE_COL],
            className: 'text-center',
            title: 'Price',
        },
        {
            data: gPET[gPROMOTION_PRICE_COL],
            className: 'text-center',
            title: 'Promotion Price',
        },
        {
            data: gPET[gDISCOUNT_COL],
            className: 'text-center',
            title: 'Discount',
        },
        {
            data: gPET[gCREATE_DATE],
            title: 'Create Date',
            render: function (data, type) {
                const date = new Date(data);

                if (type === "display") {
                    return date.toLocaleDateString('en-GB'); // 24/04/2023
                }

                return date;
            }
        },
        {
            data: gPET[gUPDATE_DATE],
            title: 'Update Date',
            render: function (data, type) {
                const date = new Date(data);

                if (type === "display") {
                    return date.toLocaleDateString('en-GB'); // 24/04/2023
                }

                return date;
            }
        },
        {
            data: 'id',
            title: 'Action',
            render: function (id, type) {
                if (type === "display") {
                    return `
                    <div class="d-flex justify-content-center">
                        <button data-id=${id} class="btn edit-pet"><i class="fa-solid fa-pen-to-square fa-pen-to-square-css" data-toggle="tooltip" data-placement="bottom" title="Edit"></i></button>
                        <button data-id=${id} class="btn delete-pet text-danger"><i class="fa-solid fa-trash-can fa-trash-can-css" data-toggle="tooltip" data-placement="bottom" title="Delete"></i></button>       
                    </div>
                `;
                }
                return id;
            }
        },

    ],
    columnDefs: [
        {
            width: "15%",
            targets: gDESCRIPTION_COL,
            className: "text-break"

        },
        {
            targets: gACTION,
            orderable: false
        },
        {
            targets: gIMAGE_URL_COL,
            visible: false
        }
    ]

})
/*** REGION 2 - Vùng gán / thực thi hàm xử lý sự kiện cho các elements */
// Hàm xử lý sự kiện tải trang
function onPageLoading() {
    $('.overlay').show();
    callApiGetPetList();

    //Date picker create date
    $('#reservationdate1').datetimepicker({
        format: 'L'
    });
    //Date picker update date
    $('#reservationdate2').datetimepicker({
        format: 'L'
    });
}

//Hàm xử lý sự kiện cho nút create 
function createNewPet() {
    clearDataModal();
    $("#modal-create-edit-pet").modal("show");
    $(".form-control").removeClass("is-invalid");
    $("#btn-create").css("display", "block");
    $("#btn-edit").css("display", "none");
    $('#input-create-edit-name').trigger('focus');
}

//Gán sự kiện cho nút sửa
$('#example1').on('click', '.edit-pet', function () {
    gPET_ID = this.dataset.id;
    $(".form-control").removeClass("is-invalid");
    //Gọi API để lấy Pet data theo ID
    callApiGetPetById();
})

//Gán sự kiện cho nút delete
$('#example1').on('click', '.delete-pet', function () {
    gPET_ID = this.dataset.id;
    $(".modal-delete").modal("show");
})

//Gán sự kiện cho nút confirm delete (modal)
$("#btn-confirm").click(function () {
    $('.overlay').show();
    //Gọi API để xóa pet theo ID
    callApiDeletePetById();
})

/*** REGION 3 - Event handlers - Vùng khai báo các hàm xử lý sự kiện */
// Hàm sửa thông tin Pet theo ID
function editPet(paramButton) {
    //Đọc dữ liệu
    var vPetObj = {
        type: "",
        name: "",
        description: "",
        imageUrl: "",
        price: 0,
        promotionPrice: 0,
        discount: 0
    };
    readData(vPetObj);
    let dataIsValidated = checkData(vPetObj);
    if (dataIsValidated) {
        $('.overlay').show();
        callApiEditPet(vPetObj);

    }
}

// Hàm xử lý sự kiện nút tạo mới pet (modal)
function createNewPetModal() {

    var vPetObject = {
        type: "",
        name: "",
        description: "",
        imageUrl: "",
        price: null,
        promotionPrice: null,
        discount: 0
    };


    //Đọc dữ liệu
    readData(vPetObject);
    // Kiểm tra dữ liệu
    console.log(vPetObject);
    let isDataValidated = checkData(vPetObject);
    if (isDataValidated) {
        //Gọi api tạo mới Pet
        callApiCreatePet(vPetObject);
    }
}
/*** REGION 4 - Common funtions - Vùng khai báo hàm dùng chung trong toàn bộ chương trình*/
// Hàm gọi api lấy danh sách Pet
function callApiGetPetList() {
    var vAPI_URL = gBASE_URL + '/pets';
    $.ajax({
        url: vAPI_URL,
        type: 'GET',
        success: function (paramSuccess) {
            gPET_LIST = paramSuccess.rows;
            displayDataToTable();
            $('.overlay').hide();
        }
    })
}

// Hàm gọi API tạo Pet mới
function callApiCreatePet(paramPetObj) {
    const vAPI_URL = gBASE_URL + '/pets';

    $.ajax({
        url: vAPI_URL,
        type: "POST",
        dataType: "JSON",
        data: JSON.stringify(paramPetObj),
        contentType: "application/json",
        success: function (result) {
            alert("Create New Pet success")
            $(".modal-create-edit").modal("hide");
            window.location.reload();
        },
        error: function (paramError) {
            console.log(paramError.responseText);
        }

    })
}

// Hàm gọi API lấy thông tin Pet theo ID
function callApiGetPetById() {
    $(".modal-create-edit").modal("show");
    $("#btn-create").css("display", "none");
    $("#btn-edit").css("display", "block");
    $('#input-create-edit-name').trigger('focus');

    const vAPI_URL = gBASE_URL + '/pets/' + gPET_ID;
    $.ajax({
        url: vAPI_URL,
        type: "GET",
        success: function (paramSuccess) {
            console.log(paramSuccess)
            $(".modal-create-edit").modal("show");
            $("#btn-create").css("display", "none");
            $("#btn-edit").css("display", "block");
            $('#input-create-edit-name').trigger('focus');
            displayDataToModal(paramSuccess);
        },
        error: function (paramError) {
            console.log(paramError.responseText)
        }
    })
}

// Hàm gọi API sửa thông tin Pet theo ID
function callApiEditPet(paramPetObj) {
    const vAPI_URL = gBASE_URL + '/pets/' + gPET_ID;
    $.ajax({
        url: vAPI_URL,
        type: "PUT",
        dataType: "JSON",
        data: JSON.stringify(paramPetObj),
        contentType: "application/json",
        success: function (result) {
            $(".modal-create-edit").modal("hide");
            $('.overlay').hide();
            setTimeout(function () {
                alert("Edit pet success")
            }, 5);

            window.location.reload();
        },
        error: function (paramError) {
            console.log(paramError.responseText);
        }

    })
}

// Hàm gọi API xóa Pet theo ID 
function callApiDeletePetById() {
    const vAPI_URL = gBASE_URL + '/pets/' + gPET_ID;
    $.ajax({
        url: vAPI_URL,
        type: "DELETE",
        success: function (paramSuccess) {
            $(".modal-delete").modal("hide");
            $('.overlay').hide();
            setTimeout(function () {
                alert("Delete Pet success")
            }, 15);
            window.location.reload();
        },
        error: function (paramError) {
            alert(paramError.responseText);
        }
    })
}

//Hàm xử lý hiển thị data lên modal create-edit
function displayDataToModal(paramPetObj) {

    $("#input-create-edit-name").val(paramPetObj.name);

    $("#input-create-edit-type").val(paramPetObj.type);

    $("#textarea-create-edit-description").val(paramPetObj.description);

    $("#input-create-edit-date").val(paramPetObj.createdAt)

    $("#input-create-edit-update-date").val(paramPetObj.updatedAt)

    $("#input-create-edit-price").val(paramPetObj.price)

    $("#input-create-edit-promotion-price").val(paramPetObj.promotionPrice)

    $("#input-create-edit-discount").val(paramPetObj.discount)

    $("#input-create-edit-ImageUrl").val(paramPetObj.imageUrl)

    $("#pet-image").attr("src", paramPetObj.imageUrl);
    $(".choose-pet-image").html("");
    $("#pet-image").css("display", "block")

}

// Hàm đọc dữ liệu trên modal create-edit
function readData(paramPetObj) {

    paramPetObj.name = $.trim($("#input-create-edit-name").val());

    paramPetObj.type = $.trim($("#input-create-edit-type").val());

    paramPetObj.description = $.trim($("#textarea-create-edit-description").val());

    // paramPetObj.createdAt = $.trim($("#input-create-edit-date").val());

    // paramPetObj.updatedAt = $.trim($("#input-create-edit-update-date").val());

    if ($("#input-create-edit-price").val() === "") {
        paramPetObj.price = null;
    } else {
        paramPetObj.price = Number(Math.round($.trim($("#input-create-edit-price").val())));
    }

    if ($("#input-create-edit-promotion-price").val() === "") {
        paramPetObj.promotionPrice = null;
    } else {
        paramPetObj.promotionPrice = Number(Math.round($.trim($("#input-create-edit-promotion-price").val())));
    }

    paramPetObj.discount = Number(Math.round($.trim($("#input-create-edit-discount").val())));

    paramPetObj.imageUrl = $("#input-create-edit-ImageUrl").val();

}

// Hàm kiểm tra dữ liệu 
function checkData(paramPetObj) {

    $(".form-control").removeClass("is-invalid");
    let isValid = true;
    //Name: Text
    if (paramPetObj.name === "") {
        $("#input-create-edit-name").addClass("is-invalid");
        $("#input-create-edit-name ~.invalid-feedback").text("Please enter name");
        isValid = false;
    }
    //Type: Text
    if (paramPetObj.type === "") {
        $("#input-create-edit-type").addClass("is-invalid");
        $("#input-create-edit-type ~.invalid-feedback").text("Please select type");
        isValid = false;
    }

    // Url Image: text 
    if (paramPetObj.imageUrl === "") {
        $("#input-create-edit-ImageUrl").addClass("is-invalid");
        $("#input-create-edit-ImageUrl ~.invalid-feedback").text("Please enter link image");
        isValid = false;
    } else if (/\.(jpg|png|gif|jpeg|svg)/gi.test(paramPetObj.imageUrl) === false) {
        $('#input-create-edit-ImageUrl').addClass("is-invalid");
        $('#input-create-edit-ImageUrl ~ .invalid-feedback').text("Pet image must be .jpg, .png, .gif, .jpeg, .svg");
        isValid = false;
    }

    //Price: whole number and > 0
    if (paramPetObj.price === null) {
        $("#input-create-edit-price").addClass("is-invalid");
        $("#input-create-edit-price ~.invalid-feedback").text("Please enter price");
        isValid = false;
    } else if (paramPetObj.price <= 0) {
        $("#input-create-edit-price").addClass("is-invalid");
        $("#input-create-edit-price ~.invalid-feedback").text("Price must be over 0");
        isValid = false;
    }

    //Promotion whole Price : number, >= 0 and <= price
    if (paramPetObj.promotionPrice === null) {
        $("#input-create-edit-promotion-price").addClass("is-invalid");
        $("#input-create-edit-promotion-price ~.invalid-feedback").text("Please enter Promotion price");
        isValid = false;
    }
    else if (paramPetObj.promotionPrice < 0 || paramPetObj.promotionPrice > paramPetObj.price) {
        $("#input-create-edit-promotion-price").addClass("is-invalid");
        $("#input-create-edit-promotion-price ~.invalid-feedback").text("Promotion price must be over or equal 0 and smaller than price");
        isValid = false;
    }

    //Discount : whole number, >= 0 and <= 100
    if (paramPetObj.discount < 0 || paramPetObj.discount > 100) {
        $("#input-create-edit-discount").addClass("is-invalid");
        $("#input-create-edit-discount ~.invalid-feedback").text("Discount must be over or equal 0 and smaller than 100");
        isValid = false;
    }

    // Description: text
    if (paramPetObj.description === "") {
        $("#textarea-create-edit-description").addClass("is-invalid");
        $("#textarea-create-edit-description ~.invalid-feedback").text("Please enter pet description");
        isValid = false;
    } else if (paramPetObj.description.length < 20) {
        $("#textarea-create-edit-description").addClass("is-invalid");
        $("#textarea-create-edit-description ~ .invalid-feedback").text("Description must be over 20 letter");
        isValid = false;
    }

    return isValid;
}

// Hàm clear hết data trên modal create-edit
function clearDataModal() {
    $("#input-create-edit-name").val("");
    $("#input-create-edit-type").val("");
    $("#textarea-create-edit-description").val("");
    $("#input-create-edit-date").val("");
    $("#input-create-edit-update-date").val("");
    $("#input-create-edit-price").val("");
    $("#input-create-edit-promotion-price").val("");
    $("#input-create-edit-discount").val("");

    $("#pet-image").attr("src", "");
    $("#pet-image").css("display", "none");
    $(".choose-pet-image").html("Pet image");
    $("#input-create-edit-ImageUrl").val("")

}

//Hàm hiển thị Pet Image
function choosePetImage(paramInput) {

    $(".choose-teacher-image").html("");
    $("#pet-image").attr("src", $("#input-create-edit-ImageUrl").val());
    $("#pet-image").css("display", "block");

    // console.log(paramInput.files[0]);
    // const vMyFile = paramInput.files[0];
    // let vPetSize = paramInput.files[0].size;

    // const vFile = Math.round((vPetSize / 1024));

    // if (vFile < 40) {
    //     var reader = new FileReader();
    //     reader.onload = function (event) {
    //         $(".choose-pet-image").html("");
    //         $("#pet-image").attr("src", event.target.result);
    //         $("#pet-image").css("display", "block");
    //     };
    //     reader.readAsDataURL(vMyFile);
    // } else {
    //     $("#label-pet-image ~ .invalid-feedback-css").text("File too big");
    //     $(".invalid-feedback-css").removeAttr("hidden");
    // }

}

//Hàm hiển thị Pet data lên table
function displayDataToTable() {

    gPetTable.clear();
    gPetTable.rows.add(gPET_LIST);
    gPetTable.draw().buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');

    $(".div-select-type").removeAttr("hidden");
    $(".min-price").removeAttr("hidden");
    $(".max-price").removeAttr("hidden");

    //Hàm chạy tooltip
    $(function () {
        $('[data-toggle="tooltip"]').tooltip()
    })
}

//Hàm gọi API lấy danh sách Pets phân trang có bộ lọc
function callApiGetPetByFilter() {
    const vQueryParams = new URLSearchParams();
    // vQueryParams.append('_limit', 8);
    // vQueryParams.append('_page', 0);

    if ($("#select-type").val() !== "") {
        vQueryParams.append('type', $("#select-type").val());
    }
    if ($('#input-min-price').val() !== 0) {
        vQueryParams.append('priceMin', $('#input-min-price').val());
    }
    if ($('#input-max-price').val() !== 0) {
        vQueryParams.append('priceMax', $('#input-max-price').val());
    }

    $.ajax({
        type: 'get',
        url: gBASE_URL + '/pets?' + vQueryParams.toString(),
        dataType: 'json',
        success: function (paramData) {
            gPET_LIST = paramData.rows;
            console.log(gPET_LIST)
            displayDataToTable();
        },
        error: function () {
            console.log(error);
        }
    });
}
